FROM alpine:edge

RUN apk update && \
    apk add --no-cache \
    # base
     bash bash-completion vim tmux \
    # network
    bind-tools iputils tcpdump curl nmap tcpflow iftop net-tools mtr netcat-openbsd bridge-utils iperf ngrep \
    # processes/io
    htop atop strace iotop sysstat ltrace ncdu logrotate hdparm pciutils psmisc tree pv

ENTRYPOINT ["tail", "-f", "/dev/null"]
